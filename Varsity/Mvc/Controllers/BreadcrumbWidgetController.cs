﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using Varsity.Mvc.Models;

namespace Varsity.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "BreadcrumbWidget", Title = "BreadcrumbWidget", SectionName = "Varsity")]
    public class BreadcrumbWidgetController : Controller
    {
        public string RootPath { get; set; }
        public string SecondPath { get; set; }

        // GET: BreadcrumbWidget
        public ActionResult Index()
        {
            string path = Request.Path.TrimStart('/').TrimEnd('/');
            string[] sub = path.Split('/');

            this.RootPath = sub[0].ToPascalCase();
            this.SecondPath = sub.Length > 1 ? sub[1].ToPascalCase() : string.Empty;

            var breadcrumb = new BreadcrumbWidget()
            {
                RootPath = this.RootPath,
                SecondPath = this.SecondPath
            };
            return View("Default", breadcrumb);
        }
    }
}