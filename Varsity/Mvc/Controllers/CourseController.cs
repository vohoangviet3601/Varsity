﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;
using Varsity.Mvc.Models;
using Varsity.Mvc.Services;

namespace Varsity.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "CourseWidget", Title = "CourseWidget", SectionName = "Varsity")]

    public class CourseController : Controller
    {
        private CourseService courseService = new CourseService();

        public ActionResult Index(int index = 1, int pageSize = 4)
        {
            List<Course> listCourse = courseService.GetAllLiveCourses();

            if (listCourse == null)
            {
                return Redirect("~/");
            }

            int count = listCourse.Count;
            listCourse = listCourse.Skip((index - 1) * pageSize).Take(pageSize).ToList();
            int maxPage = (count / pageSize) + (count % pageSize > 0 ? 1 : 0);

            ViewBag.MaxPage = maxPage;
            ViewBag.PageIndex = index;
            ViewBag.PreIndex = index > 1 ? index - 1 : 1;
            ViewBag.NextIndex = index < maxPage ? index + 1 : maxPage;

            return View("Default", listCourse);
        }

        public ActionResult Detail(string url)
        {
            Course course = courseService.GetCourseByUrl(url);

            if (course == null)
            {
                return Redirect("~/");
            }

            return View("Detail", course);
        }
    }
}