﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Varsity.Mvc.Models
{
    public class BreadcrumbWidget
    {
        public string RootPath { get; set; }
        public string SecondPath { get; set; }
    }
}