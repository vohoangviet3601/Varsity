﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.GeoLocations.Model;

namespace Varsity.Mvc.Models
{
    public class Course
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public int Price { get; set; }
        public int Duration { get; set; }
        public int Students { get; set; }
        public Address Place { get; set; }
        public HierarchicalTaxon Category { get; set; }
        public Image Image { get; set; }
        public List<Outline> outlines { get; set; }
    }

    public class Outline
    {
        public string Title { get; set; }
        public string CourseTime { get; set; }
        public string SpentTime { get; set; }
        public string Status { get; set; }
    }

    public static class StringExtension
    {
        public static string GetFirstParagraph(this string str)
        {
            var regex = new Regex(@"^.*?[.?!](?=\s+\p{P}*[\p{Lu}\p{N}]|\s*$)");
            Match match = regex.Match(str);

            return match.Success ? match.Value.Replace(@"<p>", "").Replace(@"</p>", "") : str;
        }
    } 
}