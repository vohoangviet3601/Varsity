﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.GeoLocations.Model;
using Telerik.Sitefinity.Libraries.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.RelatedData;
using Telerik.Sitefinity.Taxonomies;
using Telerik.Sitefinity.Taxonomies.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Varsity.Mvc.Models;


namespace Varsity.Mvc.Services
{
    public class CourseService
    {
        public List<Course> GetAllLiveCourses()
        {
            try
            {
                DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager();
                Type courseType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Courses.Course");
                IQueryable<DynamicContent> courseCollection = dynamicModuleManager.GetDataItems(courseType).Where(x => x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live).OrderBy(x => x.DateCreated);

                var courseList = new List<Course>();

                TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();

                var categories = taxonomyManager.GetTaxa<HierarchicalTaxon>().Where(t => t.Taxonomy.Name == "course-categories");

                foreach (var course in courseCollection)
                {
                    var temp = new Course();
                    temp.Id = course.GetValue("Id")?.ToString();
                    temp.Title = course.GetValue("Title")?.ToString();
                    temp.Description = course.GetValue("Description")?.ToString();
                    temp.StartDate = course.GetValue("StartDate") != null ? (DateTime)course.GetValue("StartDate") : new DateTime();
                    temp.Price = course.GetValue("Price") != null ? Convert.ToInt32(course.GetValue("Price")) : 0;
                    temp.Duration = course.GetValue("Duration") != null ? Convert.ToInt32(course.GetValue("Duration")) : 0;
                    temp.Students = course.GetValue("Students") != null ? Convert.ToInt32(course.GetValue("Students")) : 0;
                    temp.Place = course.GetValue("Place") != null ? (Address)course.GetValue("Place") : new Address();
                    temp.Image = course.GetRelatedItems<Image>("Image")?.FirstOrDefault();
                    temp.Url = course.UrlName.Value.ToString();

                    string categoryId = ((Telerik.OpenAccess.TrackedList<Guid>)course.GetValue("coursecategories"))[0].ToString();
                    var category = categories.FirstOrDefault(x => x.Id.ToString() == categoryId);

                    if (category != null)
                    {
                        temp.Category = category;
                    }

                    courseList.Add(temp);
                }

                return courseList;
            }
            catch (Exception ex)
            {
                Log.Write($"{nameof(GetAllLiveCourses)} | {ex.Message}");
                return null;
            }
        }

        public Course GetCourseByUrl(string url)
        {
            try
            {
                DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager();
                Type courseType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Courses.Course");
                DynamicContent courseItem = dynamicModuleManager.GetDataItems(courseType).FirstOrDefault(x => x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live && x.UrlName == url);

                var course = new Course();
                course.Id = courseItem.GetValue("Id")?.ToString();
                course.Title = courseItem.GetValue("Title")?.ToString();
                course.Description = courseItem.GetValue("Description")?.ToString();
                course.StartDate = courseItem.GetValue("StartDate") != null ? (DateTime)courseItem.GetValue("StartDate") : new DateTime();
                course.Price = courseItem.GetValue("Price") != null ? Convert.ToInt32(courseItem.GetValue("Price")) : 0;
                course.Duration = courseItem.GetValue("Duration") != null ? Convert.ToInt32(courseItem.GetValue("Duration")) : 0;
                course.Students = courseItem.GetValue("Students") != null ? Convert.ToInt32(courseItem.GetValue("Students")) : 0;
                course.Place = courseItem.GetValue("Place") != null ? (Address)courseItem.GetValue("Place") : new Address();
                course.Image = courseItem.GetRelatedItems<Image>("Image")?.FirstOrDefault();
                course.Url = courseItem.UrlName.Value.ToString();

                TaxonomyManager taxonomyManager = TaxonomyManager.GetManager();
                string categoryId = ((Telerik.OpenAccess.TrackedList<Guid>)courseItem.GetValue("coursecategories"))[0].ToString();
                var category = taxonomyManager.GetTaxa<HierarchicalTaxon>().FirstOrDefault(t => t.Taxonomy.Name == "course-categories" && t.Id.ToString() == categoryId);

                if (category != null)
                {
                    course.Category = category;
                }

                Type outlineType = TypeResolutionService.ResolveType("Telerik.Sitefinity.DynamicTypes.Model.Courses.Outline");
                IQueryable<DynamicContent> childItems = dynamicModuleManager.GetChildItems(courseItem, outlineType);
                var outlineList = new List<Outline>();

                childItems = childItems.Where(x => x.Status == Telerik.Sitefinity.GenericContent.Model.ContentLifecycleStatus.Live);

                foreach (var item in childItems)
                {
                    string statusId = ((Telerik.OpenAccess.TrackedList<Guid>)item.GetValue("coursestatuses"))[0].ToString();
                    var status = taxonomyManager.GetTaxa<HierarchicalTaxon>().FirstOrDefault(t => t.Taxonomy.Name == "course-statuses" && t.Id.ToString() == statusId);

                    if (status != null)
                    {
                        var outline = new Outline();
                        outline.Title = item.GetValue("Title").ToString();
                        outline.CourseTime = item.GetValue("CourseTime").ToString();
                        outline.SpentTime = item.GetValue("SpentTime").ToString();
                        outline.Status = status.Name.ToPascalCase();

                        outlineList.Add(outline);
                    }
                }
                course.outlines = outlineList;

                return course;
            }
            catch (Exception ex)
            {
                Log.Write($"{nameof(GetCourseByUrl)} | {ex.Message}");
                return null;
            }
        }
    }
}